import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component'; 

import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
// external
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MostrarComponent } from './component/persona/mostrar/mostrar.component';
import { FormularioComponent } from './component/persona/formulario/formulario.component';


@NgModule({
  declarations: [
    AppComponent,
    MostrarComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
