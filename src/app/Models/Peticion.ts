export class Peticion {
  status: number;
  mensaje: String;
  data: any;

  constructor(status: number, mensaje: String, data: any) {
    this.status = status;
    this.mensaje = mensaje;
    this.data = data;
  }
}
