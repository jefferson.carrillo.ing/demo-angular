import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostrarComponent } from './component/persona/mostrar/mostrar.component';
import { FormularioComponent} from './component/persona/formulario/formulario.component'; 

const routes: Routes = [
  {path: '',component:  MostrarComponent}, 
  {path:'formulario/:id',component: FormularioComponent},
  {path:'formulario',component: FormularioComponent},
  {path:'**',redirectTo:'',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
