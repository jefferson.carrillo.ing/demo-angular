import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../Models/Persona';
import { Parametro } from '../Models/Parametro';
import { Peticion } from '../Models/Peticion';

@Injectable({
  providedIn: 'root',
})
export class PersonaService {
  private _Parametro: Parametro = new Parametro();
  constructor(private _HttpClient: HttpClient) {}

  private link_mostrar = this._Parametro.url_root + '/api/persona/mostrar';
  private link_bucscar = this._Parametro.url_root + '/api/persona/buscar';
  private link_guardar = this._Parametro.url_root + '/api/persona/guardar';
  private link_eliminar = this._Parametro.url_root + '/api/persona/eliminar/?id=';

  public mostrar(): Observable<Peticion> {
    return this._HttpClient.get<Peticion>(this.link_mostrar);
  }

  public buscar(id: number): Observable<Peticion> {
    let persona = new Persona();
    persona.id = id;
    return this._HttpClient.post<Peticion>(this.link_bucscar, persona);
  }

  public guardar(persona: Persona): Observable<Peticion> {
    return this._HttpClient.post<Peticion>(this.link_guardar, persona);
  }

  public eliminar(id: number): Observable<Peticion> {
    return this._HttpClient.delete<Peticion>(this.link_eliminar + id);
  }
}
