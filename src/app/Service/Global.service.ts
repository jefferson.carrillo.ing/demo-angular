import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Peticion } from '../Models/Peticion';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  constructor(private _ToastrService: ToastrService) {}

  GetNotificarError(error: any) {
    let mensaje = (error.mensaje || error.message).toString();

    this._ToastrService.error(mensaje, 'Fail', {
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
    });
  }

  GetNotificarSuccess(data: Peticion) {
    this._ToastrService.success(data.mensaje.toString(), 'OK', {
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
    });
  }
}
