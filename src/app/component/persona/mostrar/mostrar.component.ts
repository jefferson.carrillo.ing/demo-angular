import { Component, OnInit } from '@angular/core';
import { Peticion } from 'src/app/Models/Peticion';
import { Persona } from '../../../Models/Persona';
import { PersonaService } from '../../../Service/Persona.service';
import { GlobalService } from '../../../Service/Global.service';
@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css'],
})
export class MostrarComponent implements OnInit {
  public Personas: Persona[] = [];

  constructor(
    private _PersonaService: PersonaService,
    private _GlobalService: GlobalService
  ) {}

  ngOnInit(): void {
    this.MostrarRegistro();
  }

  MostrarRegistro(): void { 
    this._PersonaService.mostrar().subscribe(
      (dato: Peticion) => { 
        this.Personas = dato.data;
       // this._GlobalService.GetNotificarSuccess(dato);
      },
      (error: Peticion) => { 
        this._GlobalService.GetNotificarError(error);
      }
    );
  }

  EliminarRegistro(id: number) {
    this._PersonaService.eliminar(id).subscribe(
      (dato: Peticion) => {
        this.MostrarRegistro();
        this._GlobalService.GetNotificarSuccess(dato);
      },

      (error: Peticion) => {
        this._GlobalService.GetNotificarError(error);
      }
    );
  }
}
