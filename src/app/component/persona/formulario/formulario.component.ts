import { Component, OnInit } from '@angular/core';
import { Peticion } from 'src/app/Models/Peticion';
import { Persona } from '../../../Models/Persona';
import { PersonaService } from '../../../Service/Persona.service';
import { GlobalService } from '../../../Service/Global.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent implements OnInit {
  public persona: Persona = new Persona();

  public tipo_form  = 0;
  constructor(
    private _PersonaService: PersonaService,
    private _GlobalService: GlobalService,
    private _Router: Router,
    private _ActivatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void { 
   this.CargarFormulario();
  }

  CargarFormulario(): void {
    const id = this._ActivatedRoute.snapshot.params.id;
    if (id != null) {
      this.tipo_form = 1;
      this._PersonaService.buscar(id).subscribe(
        (dato: Peticion) => {
          this.persona = dato.data;
        },
        (error: Peticion) => {
          this._GlobalService.GetNotificarError(error);
        }
      );
    }
  }

  GuardarFormulario(): void {
    const id = this._ActivatedRoute.snapshot.params.id;

    this.persona.id = id;
    this._PersonaService.guardar(this.persona).subscribe(
      (dato: Peticion) => {
        this.persona = dato.data;
        this._GlobalService.GetNotificarSuccess(dato);
        this._Router.navigate(['/']);
      },
      (error: Peticion) => {
        this._GlobalService.GetNotificarError(error);
      }
    );
  }
}
